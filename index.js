var express = require('express');
var app = express();
var db = require('./db.js');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.get('/', function (req, res) {
	res.send('Hello world!')
})

app.get('/testdb', function (req, res) {
	
	db.query("SELECT * FROM test", function(err,result) {
	
	if (!err) {
	   res.json(result.rows)
	} else {
	   res.send("Error a la db");
		console.log(err);
	}
})
})

app.get('/app', function (req, res) {

        db.query("SELECT * FROM app", function(err,result) {

        if (!err) {
           res.json(result.rows);
        } else {
           res.send("Error a la db");
                console.log(err);
        }
})
})

app.get('/:app_code', function (req, res) {

        db.query("SELECT * FROM record WHERE app_code = '" + req.params.app_code + "'", function(err,result) {

        if (!err) {
           res.json(result.rows);
        } else {
           res.send("Error 404");
                console.log(err);
        }
})
})

app.post('/:app_id', function (req, res) {
	
        db.query("INSERT INTO record (app_code, player, score) VALUES ('"+req.params.app_id+"', '"+req.body.player+"', "+req.body.score +")", function(err,result) {

        if (!err) {
           res.json(result.rows);
        } else {
           res.send("Error 500");
                console.log(err);
        }
})
})


app.listen(process.env.PORT || 3000, function () {
	console.log('Example');
})
